day, month, temperature = input(), input(), input()
print(f'Сегодня {day} {month}. На улице {temperature} градусов.')
if int(temperature) < 0:
    print('Холодно, лучше остаться дома')