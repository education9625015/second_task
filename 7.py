fib_num = [1, 1]
n = int(input())
if n == 1 or n == 2:
    print(fib_num[0])
elif n > 2:
    for i in range(2, n):
        fib_num.append(fib_num[i - 1] + fib_num[i - 2])
    print(fib_num[n - 1])
